// console.log(`YOW`)


function Pokemon(name, level) {
		//properties
		this.name = name;
		this.level = level;
		this.health = 3 * level;
		this.attack = level;

		this.tackle = function(target) {
			console.log(this.name + ` tackle ` + target.name)
			console.log(target.name + ` 's health is now reduce to ` + (target.health - this.attack));

			target.health -= this.attack

			if (target.health < 5) {
				target.faint()
			}

		}
		this.faint = function(){
			console.log(this.name + ` fainted.`)
		}
}

let piplup = new Pokemon(`Piplup`, 5)
let lucario = new Pokemon(`Lucario`, 8)
// console.log(piplup)
// console.log(lucario)

piplup.tackle(lucario)
piplup.tackle(lucario)
piplup.tackle(lucario)
piplup.tackle(lucario)

